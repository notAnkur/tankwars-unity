using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    [SerializeField] bool canPlayerShoot;
    float timeBetweenShot;

    Rigidbody2D rb2;
    [SerializeField] float speed;
    [SerializeField] Transform barrelPosition;

    // UI
    [SerializeField] Slider bulletInfoSlider;
    [SerializeField] Text lifeValue;
    [SerializeField] Text scoreValue;
    [SerializeField] GameObject BulletPrefab;
    [SerializeField] public int PlayerLives { get; private set; } = 3;

    private void Start()
    {
        rb2 = GetComponent<Rigidbody2D>();
        canPlayerShoot = true;
        timeBetweenShot = LevelManager.Instance.TimeBetweenShots;
        SetPlayerHealth();
    }

    void Update()
    {
        // Player movement
        float h = Input.GetAxisRaw("Horizontal");
        //float v = Input.GetAxisRaw("Vertical");
        Vector2 dir = new Vector2(h, 0).normalized;

        MovePlayer(dir);

        // Player shoot
        if(Input.GetKeyDown(KeyCode.Space))
        {
            ShootShell();
        }
    }

    private void MovePlayer(Vector2 dir)
    {
        Vector2 min = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
        Vector2 max = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));

        max.x -= 0.25f;
        min.x += 0.25f;

        Vector2 pos = transform.position;
        pos += dir * speed * Time.deltaTime;

        pos.x = Mathf.Clamp(pos.x, min.x, max.x);
        pos.y = min.y + 0.6f;

        transform.position = pos;
    }

    private void ShootShell()
    {
        if(!canPlayerShoot)
        {
            // play empty barrel sound
        } else
        {
            //Instantiate(BulletPrefab, barrelPosition, false);
            Instantiate(BulletPrefab, barrelPosition.position, Quaternion.identity);
            StartCoroutine(HandleShootBool(timeBetweenShot));
        }
    }

    public void Damage()
    {
        PlayerLives--;
        if (PlayerLives < 0) PlayerLives = 0;
        if(PlayerLives <= 0)
        {
            LevelManager.Instance.PlayerDead();
        }
        SetPlayerHealth();
    }

    private void SetPlayerHealth()
    {
        lifeValue.text = $"X{PlayerLives}";
    }

    IEnumerator HandleShootBool(float seconds)
    {
        canPlayerShoot = false;
        float startTime = Time.time;
        float timePassed = Time.time;

        while ((Time.time - startTime) < seconds)
        {
            timePassed += Time.deltaTime;
            //Debug.Log(timePassed);
            bulletInfoSlider.value = Mathf.Clamp(Time.time-startTime, 0f, 1f);
            yield return null;
        }

        canPlayerShoot = true;
    }
}
