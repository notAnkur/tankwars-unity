using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{

    public static LevelManager Instance;

    [SerializeField] bool IsPlayerAlive = true;
    [SerializeField] public bool IsGameOver { get; private set; } = false;
    [SerializeField] public float TimeBetweenShots = 1.0f;
    [SerializeField] Text ScoreTextField;
    [SerializeField] GameObject GameOverPopup;
    [SerializeField] Text GameOverInfoText;
    [SerializeField] Text GameOverScoreText;
    public int PlayerScore { get; private set; } = 0;


    private void Awake()
    {
        Instance = this;
        Time.timeScale = 1;
    }

    public void UpdateScore(int score)
    {
        PlayerScore += score;
        ScoreTextField.text = PlayerScore.ToString();
        CheckIfGameOver();
    }

    /// <summary>
    /// To be triggered if player cleared all the enemies or enemies reached the player base
    /// </summary>
    public void GameOver()
    {
        Time.timeScale = 0;
        GameOverPopup.SetActive(true);
        int enemyCount = EnemyManager.Instance.transform.childCount;
        if(enemyCount > 1)
        {
            // enemy reached the player base
            SetGameOverPopup("You Lost!! Enemies infilterated your base.");
        } else if(enemyCount <= 1)
        {
            // player cleared all the enemies
            SetGameOverPopup("Congrats, You won!!");
        }
    }

    public void PlayerDead()
    {
        Time.timeScale = 0;
        GameOverPopup.SetActive(true);
        SetGameOverPopup("You Lost!! Your tank exploded.");
    }

    /// <summary>
    /// Checks if total enemies is <= 0
    /// </summary>
    public void CheckIfGameOver()
    {
        int enemyCount = EnemyManager.Instance.transform.childCount;
        Debug.Log(enemyCount);
        if(enemyCount <= 1)
        {
            GameOver();
        }
    }

    private void SetGameOverPopup(string msg)
    {
        GameOverInfoText.text = msg;
        GameOverScoreText.text = $"Score: {PlayerScore}";
    }

    public void RestartGame()
    {
        // Reload current scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void MainMenuButton()
    {
        // Reload current scene
        SceneManager.LoadScene(0);
    }
}
