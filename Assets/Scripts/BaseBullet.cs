using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseBullet : MonoBehaviour
{
    [SerializeField] BulletType bulletType;

    private void Update()
    {
        //transform.Translate(transform.forward * Time.deltaTime);
        if(bulletType == BulletType.PlayerBullet)
        {
            // player bullet -> bottom to top
            transform.position += Time.deltaTime * 2.0f * transform.up;
        }

        if (bulletType == BulletType.EnemyBullet)
        {
            // player bullet -> top to bottom
            transform.position -= Time.deltaTime * 2.0f * transform.up;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision.transform.tag);

        if(collision.transform.tag.Equals("Hittable"))
        {
            string collisionTag = collision.transform.tag;
            if(bulletType==BulletType.PlayerBullet)
            {
                // this is player bullet hitting the enemy, damage the enemy
                collision.transform.GetComponent<Enemy>().Damage();
                Destroy(this.gameObject);
            } else if(bulletType==BulletType.EnemyBullet)
            {
                // this is enemy bullet hitting the player, damage the player
                collision.transform.GetComponent<PlayerController>().Damage();
                Destroy(this.gameObject);
            }
        }

        if(collision.transform.GetComponent<BaseBullet>())
        {
            // bullets collided with each other
            // destroy both
            Destroy(this.gameObject);
            Destroy(collision.gameObject);
        }

        if(collision.transform.CompareTag("Obstacles"))
        {
            if(bulletType == BulletType.EnemyBullet)
            {
                // destroy bullet and the obstacle
                Destroy(this.gameObject);
                Destroy(collision.gameObject);
            }
            if(bulletType == BulletType.PlayerBullet)
            {
                // just destroy the bullet
                Destroy(this.gameObject);
            }
        }
    }
}

public enum BulletType
{
    PlayerBullet,
    EnemyBullet
}
