using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    // private
    [SerializeField] List<Transform> barrelTransforms;
    [SerializeField] float timeBetweenShots;
    [SerializeField] GameObject bulletPrefab;
    [SerializeField] int health;
    [SerializeField] int score;

    // public
    [SerializeField] public bool canShoot;

    void Start()
    {
        EnemyManager.Instance.AvailableEnemies.Add(this);
        canShoot = true;
    }

    public void Shoot()
    {
        if (!canShoot)
        {
            return;
        }
        else
        {
            foreach (Transform bT in barrelTransforms)
            {
                //Instantiate(bulletPrefab, bT, false);
                Instantiate(bulletPrefab, bT.position, Quaternion.identity);
                StartCoroutine(HandleShootBool(timeBetweenShots));
            }
        }
        if(EnemyManager.Instance.AvailableEnemies.Contains(this))
        {
            EnemyManager.Instance.AvailableEnemies.Remove(this);
        } else
        {
            Debug.Log("Enemy doesn't exist in the available list");
        }
    }

    public void Damage()
    {
        health--;
        if(health <= 0)
        {
            if (EnemyManager.Instance.AvailableEnemies.Contains(this))
            {
                EnemyManager.Instance.AvailableEnemies.Remove(this);
            }
            LevelManager.Instance.UpdateScore(score);
            Destroy(this.gameObject);
        }
    }

    IEnumerator HandleShootBool(float seconds)
    {
        yield return new WaitForSeconds(seconds);

        canShoot = true;
        if (!EnemyManager.Instance.AvailableEnemies.Contains(this))
        {
            EnemyManager.Instance.AvailableEnemies.Add(this);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag("GameOver"))
        {
            LevelManager.Instance.GameOver();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.CompareTag("Obstacles"))
        {
            this.Damage();
            Destroy(collision.gameObject);
        }
    }
}
