using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public static EnemyManager Instance;
    public List<Enemy> AvailableEnemies;

    bool canShoot;
    [SerializeField] bool moveRight;
    [SerializeField] float speed;

    private void Awake()
    {
        Instance = this;
        canShoot = true;
    }

    private void Update()
    {
        if(canShoot)
        {
            StartCoroutine(EnemyShoot());
        }

        MoveEnemies();
    }

    public void MoveEnemies()
    {
        if (this.transform.position.x <= -6.8)
        {
            // move the enemy down
            Vector2 newPos = new Vector2(transform.position.x, transform.position.y - 0.2f);
            transform.position = newPos;
            moveRight = true;
        }
        if (this.transform.position.x >= 6.8)
        {
            Vector2 newPos = new Vector2(transform.position.x, transform.position.y - 0.2f);
            transform.position = newPos;
            moveRight = false;
        }

        if(moveRight)
        {
            transform.Translate(speed * Time.deltaTime, 0, 0);
        } else
        {
            transform.Translate(-speed * Time.deltaTime, 0, 0);
        }
    }


    // make one random enemy shoot every 4 seconds
    IEnumerator EnemyShoot()
    {
        canShoot = false;
        if(AvailableEnemies.Count > 0)
        {
            int randomInt = Random.Range(0, AvailableEnemies.Count - 1);
            AvailableEnemies[randomInt].Shoot();
        }

        yield return new WaitForSeconds(4.0f);

        canShoot = true;
    }
}
